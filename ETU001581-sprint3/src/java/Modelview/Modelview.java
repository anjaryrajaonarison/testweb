/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelview;

import java.util.HashMap;

/**
 *
 * @author Anjary
 */
public class Modelview {
    HashMap hashmap;
    String page;

    public HashMap getHashmap() {
        return hashmap;
    }

    public void setHashmap(HashMap hashmap) {
        this.hashmap = hashmap;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }
}
