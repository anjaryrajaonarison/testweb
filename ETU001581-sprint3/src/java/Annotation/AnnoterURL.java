/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 *
 * @author Anjary
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface AnnoterURL {
    //String url() default "";
    String methode() default "";
}
