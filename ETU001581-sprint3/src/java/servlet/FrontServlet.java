/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import Modelview.Modelview;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Vector;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modele.Personne;

/**
 *
 * @author Anjary
 */
public class FrontServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            try {
                while(this.getServletContext().getAttribute("url")==null){
                    this.getServletContext().setAttribute("url",Utilitaire.linkMapping());
                }
                String url=Utilitaire.retriveUrlFromRawUrl(request.getRequestURI());
                out.println("link is="+url);
                Utilitaire util=new Utilitaire();
                String val=util.checkurlclass(url);
                Class classe=util.existclass(val);
                Method methode=util.checkurlmethod(url);
                out.println("ito le classe:"+classe.getName());
                out.println("ito le methode:"+methode.getName());
                Object o=classe.newInstance();
                Modelview modelview=new Modelview();
                HashMap<String,Object> setvaleur=new HashMap();
                modelview=(Modelview)methode.invoke(o);
                /*Sprint 3  */
                  if(request.getParameterNames()!=null){
                     Enumeration nameattribut =  request.getParameterNames();
                     Vector<String> listAttribute = new Vector<String>(1,1);
                     while(nameattribut.hasMoreElements()){
                          listAttribute.add(request.getParameter((String)nameattribut.nextElement()));
                     }
                     if(listAttribute.size()>0){
                       Object object=util.controldata(modelview.getHashmap().get("data"), listAttribute);
                       if(object==null){
                         out.println("salksdlakdlkdladlk");
                       }
                        setvaleur.put("data", object);
                        modelview.setHashmap(setvaleur);
                     }
                  }
                 /*fin*/
                //String rep=(String)methode.invoke(o);
                //out.println(rep+"ooooo");
                request.setAttribute("exemple", modelview);
                RequestDispatcher dispa = request.getRequestDispatcher(modelview.getPage()+".jsp");
                dispa.forward(request, response);
            } catch (Exception e) {
                 out.println("erreur: "+e.getMessage());
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
