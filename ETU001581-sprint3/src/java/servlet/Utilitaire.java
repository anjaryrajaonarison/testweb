/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import Annotation.AnnoterURL;
import Exception.UrlNotSupportedException;
import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.HashMap;
import java.util.Vector;
import modele.Personne;

/**
 *
 * @author Anjary
 */
public class Utilitaire {
    /*sprint1 feature 1 et 2*/
    static HashMap<String,HashMap<String,String>> linkMapping() {
        HashMap<String,HashMap<String,String>> resultat = new HashMap<String,HashMap<String,String>>();
        return resultat;
    }
    static String retriveUrlFromRawUrl(String rawUrl){
        return rawUrl.split("/")[rawUrl.split("/").length-1].split(".do")[0]; 
    }
    
    /*sprint2 feature 1 */
    public String getBrowser() throws Exception{
        URL location=Utilitaire.class.getProtectionDomain().getCodeSource().getLocation();
        String name=location.getFile();
        return name;
    }
    public Vector<String> searchClassurlannoter(String packagename) throws Exception
    {
         Vector<String> val=null;
         File file=null;
         String path=this.getBrowser();
         try{
             val=new Vector<String>();
             file=new File(path.concat(packagename));
             String[] name=file.list();
             for(int i=0;i<name.length;i++){
                 name[i].toCharArray();
                 if(name[i].endsWith(".class")){
                     //val.add(name[i].substring(, i))
                      name[i]=name[i].substring(0,name[i].toCharArray().length-6);
                      val.add(name[i]);
                 }
             }
         }
         catch(Exception e){
              throw e;
         }
         return val;
    }
    public HashMap<String,Object> getstock() throws Exception{
        HashMap<String,Object> val=null;
        Vector<String> classes=null;
        Class classe=null;
        Vector<Method>  methodes=null;
        Vector<String>  nameclass=null;
        Vector<String>  nameannotationm=null;
        int a=0;
        try{
            
            classes=this.searchClassurlannoter("controller");
            val=new HashMap<String,Object>();
            methodes=new Vector<Method>();
            nameclass=new Vector<String>();
            nameannotationm=new Vector<String>();
            for(int i=0;i<classes.size();i++){
                  classe=Class.forName("controller."+classes.get(i));
                  for(Method methode:classe.getDeclaredMethods()){
                      if(methode.getAnnotation(AnnoterURL.class)!=null){
                          methodes.add(methode);
                          nameannotationm.add(methode.getAnnotation(AnnoterURL.class).methode());
                          nameclass.add(classe.getSimpleName());
                          val.put("mymethod",methodes);
                          val.put("myclass",nameclass);
                          val.put("myannotationm",nameannotationm);
                      }
                  }
            }
            
        }
        catch(Exception e){
            throw e;
        }
        return val;
    }
    /*sprint2 feature 2*/
    public Method checkurlmethod(String url) throws Exception,ClassNotFoundException{
        Method method=null;
        HashMap<String,Object> stock=null;
        Vector<Method> methode=null;
        Vector<String> annotationurl=null;
        try{
            stock=this.getstock();
            methode=(Vector<Method>)stock.get("mymethod");
            annotationurl=(Vector<String>)stock.get("myannotationm");
            for(int i=0;i<annotationurl.size();i++){
                  if(annotationurl.get(i).equalsIgnoreCase(url)){
                       method=methode.get(i);
                  }
            }
        }
        catch(Exception e){
             throw new ClassNotFoundException("methode introuvable");
        }
        return method;
    }
    public String checkurlclass(String url) throws Exception,ClassNotFoundException{
        String val=null;
        HashMap<String,Object> stock=null;
        Vector<String> classe=null;
        Vector<String> annotationurl=null;
        System.out.println("exemple");
        try{
           stock=this.getstock();
           classe=(Vector<String>)stock.get("myclass");
           annotationurl=(Vector<String>)stock.get("myannotationm");
           for(int i=0;i<annotationurl.size();i++){
                System.out.println(annotationurl.get(i)+"makaiza");
                if(annotationurl.get(i).equalsIgnoreCase(url)){
                     val=classe.get(i);
                     System.out.println("testexzxsx");
                }
           }
        }
        catch(Exception e){
           //System.out.println(e.getMessage());
        }
        return val;
    }
    public Class existclass(String url) throws Exception,ClassNotFoundException{
        Class val=null;
        try{
            val=Class.forName("controller."+url);
        }catch(ClassNotFoundException e){
             val=null;
             throw new UrlNotSupportedException();
        }
        return val;
    }
    /* sprint3 */
    public  Object controldata(Object modele,Vector<String> data)  throws Exception{
        Object val=null;
        Field[] fields=null;
        try{
            //fields=modele.getClass().getDeclaredFields();
            val=Class.forName(modele.getClass().getName()).newInstance();
            fields=val.getClass().getDeclaredFields();
            System.out.println(val.getClass().getSimpleName());
            //System.out.println(fields.length+"isa champs");
            System.out.println(data.size()+"isa attribut");
            if(fields.length==data.size()){
                  for(int i=0;i<data.size();i++){
                       fields[i].setAccessible(true);
                       if(fields[i].getType().getName().equalsIgnoreCase("java.lang.String")){
                               fields[i].set(val,new String(data.get(i)));
                       }
                       if(fields[i].getType().getName().equalsIgnoreCase("int")){
                               fields[i].set(val,Integer.parseInt(data.get(i)));
                       }
                       if(fields[i].getType().getName().equalsIgnoreCase("double")){
                             fields[i].set(val,Double.parseDouble(data.get(i)));
                       }
                       //System.out.println(fields[i].get(modele));
                  }
            }
        }
        catch(Exception e){
            throw e;
        }
        return val;
    }
    public static void main(String[] args) throws Exception,ClassNotFoundException{
          /*Utilitaire utile=new Utilitaire();
          Vector<String> data=new Vector<String>();
          data.add("RAKOTOARISONA");
          data.add("Tiavina");
          data.add("19");
          Personne p=new Personne();
          Object object=utile.controldata(p,data);
          //Field[] fields=utile.controldata(p,data);
          Personne p1=(Personne)object;
          System.out.println(p1.getNom());
          System.out.println(p1.getPrenom());
          System.out.println(p1.getAge());
          /*for(Field field:fields){
                   field.setAccessible(true);
                   System.out.println(field.get(p));
          }*/
          /*String val="controller";
          Vector<String> vect=utile.searchClassurlannoter(val);
          for(int i=0;i<vect.size();i++){
              System.out.println(vect.get(i));
          }
          HashMap<String,Object> teste=utile.getstock();
           Vector<Method> methodes=(Vector<Method>)teste.get("mymethod");
           Vector<String> classes=(Vector<String>)teste.get("myclass");
           Vector<String> annotation=(Vector<String>)teste.get("myannotationm");
           for(int i=0;i<methodes.size();i++){
               
           }
           for(int i=0;i<methodes.size();i++){
               //System.out.println(methodes.get(i).getName());
               //System.out.println(classes.get(i));
               System.out.println(annotation.get(i));
           }
           try{
               //System.out.println(utile.checkurlclass("afficher")+"ito le classe");
               String val1=utile.checkurlclass("afficher");
               Class classess=utile.existclass(val1);
               Method methode=utile.checkurlmethod("afficher");
               System.out.println(methode.getName());
           }
           catch(Exception e){
               System.out.println(e.getMessage());
           }
          */
          Utilitaire util=new Utilitaire();
          System.out.println(util.getBrowser().concat("controller"));
          Vector<String> classes=util.searchClassurlannoter("controller");
          for(int a=0;a<classes.size();a++){
              System.out.println(classes.get(a));
          }
    } 
}
