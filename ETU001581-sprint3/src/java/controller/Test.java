/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import Annotation.AnnoterURL;
import Modelview.Modelview;
import java.util.HashMap;
import java.util.Vector;
import modele.Personne;

/**
 *
 * @author Anjary
 */
public class Test {
   public  HashMap<String,Object> gethashmap(){
       return new HashMap();
   }
   public Modelview getModeleview(){
       return new Modelview();
   }
   @AnnoterURL(methode="affichage")
   public Modelview testkely(){
        Modelview val=this.getModeleview();
        HashMap<String,Object> valeur =this.gethashmap();
        String data="salut tout le monde";
        valeur.put("data",data);
        val.setHashmap(valeur);
        val.setPage("first");
        return val;
   }
   
   @AnnoterURL(methode="lister")
   public Modelview lister(){
       Modelview val=this.getModeleview();
       HashMap<String,Object> valeur =this.gethashmap();
       Vector<String> liste=new Vector<String>();
       liste.add("rakoto");
       liste.add("ranaivo");
       liste.add("jean");
       valeur.put("data",liste);
       val.setHashmap(valeur);
       val.setPage("liste");
       return val;
   }
   /*sprint 3 */
   @AnnoterURL(methode="redirect")
   public Modelview makany()
   {   
       Modelview val=this.getModeleview();
       HashMap<String,Object> valeur =this.gethashmap();
       val.setPage("formulaire");
       return val;
   }
   
   @AnnoterURL(methode="formulaire")
   public Modelview traitement(){
       Modelview val=this.getModeleview();
       Personne perso=new Personne();
       HashMap<String,Object> valeur =this.gethashmap();
       val.setPage("formulairedata");
       valeur.put("data",perso);
       val.setHashmap(valeur);
       return val;
   }
} 
