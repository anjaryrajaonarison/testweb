<%-- 
    Document   : liste.jsp
    Created on : 18 nov. 2022, 11:21:37
    Author     : Anjary
--%>
<%@page import="java.util.Vector"%>
<%@page import="Modelview.Modelview"%>
<%
    Modelview model=(Modelview)request.getAttribute("exemple");
      Vector<String> vector=(Vector<String>)model.getHashmap().get("data");
%>
  
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>liste nom de personne</h1>
        <table width="100" height="100" border="1">
            <tr>
                <th>nom</th>
            </tr>
            <% for(int i=0;i<vector.size();i++){ %>
             <tr>
                <td><% out.println(vector.get(i)); %></td>
             </tr>
            <% } %>
        </table>    
    </body>
</html>
